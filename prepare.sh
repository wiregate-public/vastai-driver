#!/usr/bin/env bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.
set -eo pipefail

# trap any error, and mark it as a system failure.
trap "exit ${SYSTEM_FAILURE_EXIT_CODE}" ERR


search_offer () {
    local response=$(curl --silent --show-error --fail --location \
        --request POST "${VASTAI_API_BASEURL}/bundles/" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}" \
        --data-raw '{
            "disk_space":{"gte":16},
            "gpu_ram": {"gte":12},
            "inet_down": {"gte":400},
            "verified":{"eq":true},
            "external": {"eq": false},
            "rentable":{"eq":true},
            "rented":{"eq": false},
            "cuda_max_good":{"gte":"12.1"},
            "sort_option":{"0":["dph_total","asc"],"1":["total_flops","asc"]},
            "order":[["dph_total","asc"],["total_flops","asc"]],
            "gpu_name": {"in": ["RTX 3060", "RTX 3070"]},
            "limit":10,
            "type":"on-demand"
            }')

    local runner_num=$(expr ${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID:-0} + 0)
    local count=0
    local offer_id=""

    while IFS= read -r line; do
        if echo "$line" | grep -q '"id"'; then
            if [ $count -eq $runner_num ]; then
                offer_id=$(echo "$line" | sed -n 's/.*"id": \([0-9]*\).*/\1/p')
                break
            fi
            count=$((count + 1))
        fi
    done <<< "$(echo "$response" | tr ',' '\n')"

    echo "$offer_id"
}


start_container () {
    echo "Checking existing instances"
    # Check for idle instance
    local instance_label_idle="${CONTAINER_ID}-IDLE"
    local idle_instances=$(find_instance_by_label "${instance_label_idle}")
    if [ -n "$idle_instances" ]; then
        while IFS= read -r INST_ID; do
            echo "$INST_ID" 
            if check_instance_running "${INST_ID}"; then
                echo "Reusing existing idle instance: ${INST_ID}"
                new_label="${CONTAINER_ID}-${CUSTOM_ENV_CI_JOB_ID}"
                change_instance_label "${INST_ID}" "${new_label}"
                SSH_KEY="/tmp/vast${INST_ID}.pem"
                if [ ! -f "${SSH_KEY}" ];then
                    attach_ssh_key
                fi
                INSTANCE_ID="${INST_ID}"    
                break
            fi
        done <<< "${idle_instances}"
    fi
    while [ -z "${INSTANCE_ID}" ]; do
        echo "No valid idle instance found. Creating new instance."
        OFFER_ID=$(search_offer)
        echo "Found offer ${OFFER_ID}"
        echo "${CONTAINER_ID}-${CUSTOM_ENV_CI_JOB_ID}"
        INSTANCE_ID=$(create_instance "${CONTAINER_ID}-${CUSTOM_ENV_CI_JOB_ID}")

        if [ -n "${INSTANCE_ID}" ]; then
            wait_for_instance
            SSH_KEY="/tmp/vast${INSTANCE_ID}.pem"
            attach_ssh_key
            install_dependencies
        else
            # try again
            echo "Failed to create instance. Retrying..."
            sleep 1
        fi
    done
}


create_instance () {
    local new_label="$1"
    local dataRaw=$(echo '{
            "client_id": "me",
            "image": "'${CUSTOM_ENV_CI_JOB_IMAGE}'",
            "env": {
                "TZ": "UTC"
            },
            "price": null,
            "disk": 20,
            "label": "'${new_label}'",
            "extra": null,
            "runtype": "ssh",
            "image_login": "'${IMAGE_LOGIN}'",
            "python_utf8": false,
            "lang_utf8": false,
            "use_jupyter_lab": false,
            "jupyter_dir": null,
            "create_from": null,
            "force": false
        }')
    local response=$(curl --silent --show-error --fail --location \
        --request PUT "${VASTAI_API_BASEURL}/asks/${OFFER_ID}/" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}" \
        --data-raw "${dataRaw}")
    echo "${response}" | sed -n 's/.*"new_contract": \([0-9]*\).*/\1/p'
}


wait_for_instance () {
    local seconds=0
    local timeout=300
    while [ ${seconds} -lt ${timeout} ]; do
        local response=$(curl --silent --show-error --fail --location \
            --request GET "${VASTAI_API_BASEURL}/instances/${INSTANCE_ID}/" \
            --header 'Accept: application/json' \
            --header "Authorization: Bearer ${VASTAI_API_TOKEN}")
        if echo "${response}" | grep -q '"actual_status": "running"'; then
            echo "Instance ${INSTANCE_ID} is up and running."
            break
        else
            echo "Waiting for instance ${INSTANCE_ID} to finish loading..."
        fi
        if [ ${seconds} -ge ${timeout} ]; then
            echo "Timed out waiting for instance ${INSTANCE_ID} to be ready."
            exit 1
        fi
        seconds=$((seconds+10))
        sleep 10
    done
}

generate_ssh_key () {
    local keyPath="${1:-/tmp/vast${INSTANCE_ID}.pem}"
    if [ ! -f "${keyPath}" ];then
        ssh-keygen -t ed25519 -C "${INSTANCE_ID}" -f "${keyPath}" -q -N ""
    fi
    ssh-keygen -y -f "${keyPath}"
}

attach_ssh_key () {
    local key=$(generate_ssh_key "${SSH_KEY}")
    local response=$(curl --silent --show-error --fail --location \
        --request POST \
        "${VASTAI_API_BASEURL}/instances/${INSTANCE_ID}/ssh/" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}" \
        --data-raw '{"ssh_key": "'"${key}"'"}')
    if echo "${response}" | grep -q "key added"; then
        echo "Key added successfully. Checking instance readiness..."
        for i in {1..200}; do
            echo "Checking... ($i)"
            echo "${VASTAI_API_BASEURL}/instances/${INSTANCE_ID}"
            local response=$(curl --silent --show-error --fail --location \
                --request GET "${VASTAI_API_BASEURL}/instances/${INSTANCE_ID}" \
                --header 'Accept: application/json' \
                --header "Authorization: Bearer ${VASTAI_API_TOKEN}")
            if echo "${response}" | grep -q '"actual_status": "running"'; then
                echo "Instance is ready!"
                sleep 30
                break
            fi
            sleep 10
        done
    else
        echo "Failed to add key, but continuing..."
    fi
}

check_instance_running() {
    local instance_id="$1"
    local response=$(curl --silent --show-error --fail --location \
        --request GET "${VASTAI_API_BASEURL}/instances/${instance_id}" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}")
    echo "${response}" | grep -q '"actual_status": "running"'
}

install_dependencies () {
    # Install Git LFS, git comes pre-installed with ubuntu image.
    execute_command 'curl -s "https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh" | bash'
    execute_command "apt-get install -y git-lfs"
    # Install gitlab-runner binary since we need for cache/artifacts.
    execute_command 'curl -L --output /usr/local/bin/gitlab-runner \
    "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"'
    execute_command "chmod +x /usr/local/bin/gitlab-runner"
}

echo "Running in ${CONTAINER_ID}"
start_container