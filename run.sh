#!/usr/bin/env bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

INSTANCE_ID=$(find_instance_by_label "${CONTAINER_ID}-${CUSTOM_ENV_CI_JOB_ID}")
SSH_KEY="/tmp/vast${INSTANCE_ID}.pem"
execute_command "$(cat ${1})"
if [ $? -ne 0 ]; then
    exit ${BUILD_FAILURE_EXIT_CODE}
fi