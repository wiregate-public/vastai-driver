#!/usr/bin/env bash

check_pending_jobs () {
    local response=$(curl --silent --header "PRIVATE-TOKEN: ${CUSTOM_ENV_CI_GITLAB_PROCESSING_TOKEN}" \
        "${CUSTOM_ENV_CI_API_V4_URL}/projects/${CUSTOM_ENV_CI_PROJECT_ID}/jobs?scope=pending&stage=transcribe")
    echo "${response}" | grep -o '"id":[0-9]*' | awk -F':' '{print $2}'
}


check_running_jobs () {
    local response=$(curl --silent --header "PRIVATE-TOKEN: ${CUSTOM_ENV_CI_GITLAB_PROCESSING_TOKEN}" \
        "${CUSTOM_ENV_CI_API_V4_URL}/projects/${CUSTOM_ENV_CI_PROJECT_ID}/jobs?scope=running&stage=transcribe")
    echo "${response}" | grep -o '"id":[0-9]*' | awk -F':' '{print $2}'
}

remove_instance () {
    local DEL_INST_ID="$1"
    curl --silent --show-error --fail --location \
        --request DELETE "${VASTAI_API_BASEURL}/instances/${DEL_INST_ID}/" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}"
}

find_instance_by_label () {
    local label="$1"
    local response=$(curl --silent --show-error --fail --location \
        --request GET "${VASTAI_API_BASEURL}/instances" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}")
    local split=$(echo "${response}" | sed -e "s/}, {/\n/g")
    echo "${split}" | grep "${label}" | \
        sed -n 's/.*"id": \([0-9]*\).*/\1/p' || true
}


find_instances_by_status () {
    local partial_label="$1"
    local response=$(curl --silent --show-error --fail --location \
    --request GET "${VASTAI_API_BASEURL}/instances" \
    --header 'Accept: application/json' \
    --header "Authorization: Bearer ${VASTAI_API_TOKEN}")

    local split=$(echo "${response}" | sed -e "s/}, {/\n/g")

    echo "${split}" | grep "\"label\": \"[^\"]*${partial_label}[^\"]*\"" | \
    sed -n 's/.*"id": \([0-9]*\).*/\1/p' || true
}


get_ssh_url () {
    local response=$(curl --silent --show-error --fail --location \
        --request GET "${VASTAI_API_BASEURL}/instances/${INSTANCE_ID}/" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}")
    local ssh_host=$(echo "${response}" | \
    sed -n 's/.*"ssh_host": "\([^"]*\).*/\1/p')
    local ssh_port=$(echo "${response}" | \
    sed -n 's/.*"ssh_port": \([0-9]*\).*/\1/p')
    echo "ssh://root@${ssh_host}:${ssh_port}"
}


execute_command () {
    local cmd="${1}"
    if [ -z "${SSH_URL}" ]; then
        SSH_URL=$(get_ssh_url)
    fi
    ssh -o StrictHostKeyChecking=no -i "${SSH_KEY}" "${SSH_URL}" /bin/bash <<EOF
    ${cmd}
EOF
}


change_instance_label () {
    local instance_id="$1"
    local new_label="$2"
    local data_raw='{
        "label": "'"${new_label}"'"
    }'
    curl --silent --show-error --fail --location \
        --request PUT "${VASTAI_API_BASEURL}/instances/${instance_id}/" \
        --header 'Accept: application/json' \
        --header "Authorization: Bearer ${VASTAI_API_TOKEN}" \
        --data-raw "${data_raw}"
}


PROJECT_MASK="runner-${CUSTOM_ENV_CI_RUNNER_ID}-project-\
${CUSTOM_ENV_CI_PROJECT_ID}-concurrent"

CONTAINER_ID="runner-${CUSTOM_ENV_CI_RUNNER_ID}-project-\
${CUSTOM_ENV_CI_PROJECT_ID}-concurrent-${CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID}"

VASTAI_API_TOKEN="${CUSTOM_ENV_VASTAI_API_TOKEN}"
VASTAI_API_BASEURL="https://console.vast.ai/api/v0"
GITLAB_URL="${CUSTOM_ENV_GITLAB_URL}"

IMAGE_LOGIN="-u ${CUSTOM_ENV_IMAGE_LOGIN_USER} -p \
${CUSTOM_ENV_IMAGE_LOGIN_PASSWORD} ${CUSTOM_ENV_IMAGE_REGISTRY}"

echo "${IMAGE_LOGIN}"