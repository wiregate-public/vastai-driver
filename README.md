# Gitlab CI Runner Vast.ai Driver

Repo contains scripts that are used to configure custom executor.

More info: https://docs.gitlab.com/runner/executors/custom.html

# Requirements
- Driver requires `VASTAI_API_TOKEN`, `GITLAB_PROCESSING_TOKEN` secret variables to be defined in project;
- Driver requires `GITLAB_URL` variable with Gitlab DNS-name to be defined in project;
- Driver requires `IMAGE_LOGIN_USER`, `IMAGE_LOGIN_PASSWORD`, `IMAGE_REGISTRY` secret variables to get access to private docker registry (`-u <user> -p <password> <docker registry>`) to be defined in project;
- Driver uses `image` parameter in job definition to start docker image;
- Only `Ubuntu` based Docker images are supported;

# Usage

1. Register a new gitlab runner;
2. Clone this repo into `/opt` directory;
3. Update `config.toml`;

## Gitlab CI runner config.toml

#### The driver is configured by default to reuse the vast.io instance if there is a next job in the queue. 
#### For this functionality to work correctly, you must set `concurrent=1` in `config.toml`

```
concurrent = 1
[[runners]]
  name = "vastai-driver"
  url = "https://gitlab.com"
  token = "xxxxxxxxxxx"
  executor = "custom"
  builds_dir = "/builds"
  cache_dir = "/cache"
  [runners.custom]
    prepare_exec = "/opt/vastai-driver/prepare.sh"
    run_exec = "/opt/vastai-driver/run.sh"
    cleanup_exec = "/opt/vastai-driver/cleanup.sh"
```
