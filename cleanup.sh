#!/usr/bin/env bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

INST_ID=$(find_instance_by_label "${CONTAINER_ID}-${CUSTOM_ENV_CI_JOB_ID}")
idle_label="${CONTAINER_ID}-IDLE"
change_instance_label "${INST_ID}" "${idle_label}"

pending_job_ids=$(check_pending_jobs)
if [[ -n "$pending_job_ids" ]]; then
    echo "Pending jobs detected. Not deleting the instances to reuse for the next job."
else
    echo "No pending jobs, safely deleting the instances."
    idle_instances=$(find_instance_by_label "${idle_label}")
    if [ -n "$idle_instances" ]; then
        while IFS= read -r TM_INST_ID; do
            remove_instance "${TM_INST_ID}"
        done <<< "${idle_instances}"
    fi
    running_job_ids=$(check_running_jobs)
    if [[ -n "${running_job_ids}" ]]; then
        echo "Running jobs detected. Not deleting the instances."
    else
        all_instances=$(find_instances_by_status "${PROJECT_MASK}")
        if [ -n "${all_instances}" ]; then
            while IFS= read -r TM_INST_ID; do
                remove_instance "${TM_INST_ID}"
            done <<< "${all_instances}"
        fi    
    fi
fi
